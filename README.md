# Huntflow API

## Preprocessing
- Create a .env file in the main project directory like:
```dotenv
AUTHORIZATION='HUNTFLOW API KEY'
ACCOUNT='HUNTFLOW ORGANIZATION ID'
NGROK_AUTHTOKEN='NGROK AUTH TOKEN'
```
- Run docker-compose up
- Go to localhost:4040
- Set webhook urls like ngrok_domain/vacancies, ngrok_domain/applicants
