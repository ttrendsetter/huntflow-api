import aiohttp

from root.settings import config


class Context:
    def __init__(self):
        self.headers = {
            'authorization': config.authorization.get_secret_value(),
            'accept': 'application/json'
        }
        self.account = config.account
        self.base_url = f'https://dev-100-api.huntflow.dev/v2/accounts/{self.account}'

        self.search_applicants_url = f'{self.base_url}/applicants/search'
        self.get_statuses_url = f'{self.base_url}/vacancies/statuses'
        self.get_all_tags = f'{self.base_url}/tags'
        self.applicant_vac_url = f'{self.base_url}/applicants/app_id/vacancy'
        self.applicant_tag_url = f'{self.base_url}/applicants/app_id/tags'



context = Context()
