import datetime
from typing import Optional

from pydantic import BaseModel


class VacancyLog(BaseModel):
    id: int
    state: str


class Vacancy(BaseModel):
    id: int
    position: Optional[str]
    company: Optional[str]
    state: Optional[str]


class Status(BaseModel):
    id: int
    name: str


class Applicant(BaseModel):
    id: int


class ApplicantLog(BaseModel):
    id: int
    type: str
    status: Optional[Status]


class VacancyEvent(BaseModel):
    vacancy: Vacancy
    vacancy_log: VacancyLog


class ApplicantEvent(BaseModel):
    applicant: Applicant
    applicant_log: ApplicantLog
