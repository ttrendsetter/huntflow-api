from pydantic import BaseSettings, SecretStr


class Config(BaseSettings):
    authorization: SecretStr
    account: int

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'


config = Config()
