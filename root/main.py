from typing import Optional
import json

from root.context import context
from root.schemas import Vacancy
import aiohttp


async def get_session() -> aiohttp.ClientSession:
    async with aiohttp.ClientSession() as session:
        yield session


async def get_all_statuses(session: aiohttp.ClientSession) -> list[dict]:
    # Getting all available statuses
    response = await session.get(
        url=context.get_statuses_url,
        headers=context.headers
    )
    statuses = await response.json()
    return statuses['items']


async def get_hired_tag(session: aiohttp.ClientSession) -> Optional[int]:
    # Getting applicant tag with name HIRED
    response = await session.get(url=context.get_all_tags, headers=context.headers)
    response = await response.json()
    try:
        return [tag['id'] for tag in response['items'] if tag['name'] == 'Hired'][0]
    except IndexError:
        return


async def add_first_applicant(
        applicant: dict,
        vacancy: Vacancy,
        session: aiohttp.ClientSession
):
    # Adding the first applicant on vacancy
    statuses = await get_all_statuses(session)
    init_status = statuses[0]

    data = json.dumps({
        'vacancy': vacancy.id,
        'status': init_status['id'],
        'comment': 'Auto add',
    })

    url = context.applicant_vac_url.replace('app_id', str(applicant['id']))

    await session.post(url=url, headers=context.headers, data=data)


async def get_first_available_applicant(
        vacancy: Vacancy,
        session: aiohttp.ClientSession
) -> Optional[dict]:
    # Getting all applicants without vacancies
    params = {'q': vacancy.position, 'field': 'position', 'vacancy': 'null'}
    response = await session.get(
        url=context.search_applicants_url,
        headers=context.headers,
        params=params
    )
    applicants = await response.json()
    try:
        return applicants.get('items')[0]
    except IndexError:
        return None


async def set_applicant_tag(applicant_id: int, tag: int, session: aiohttp.ClientSession):
    # Setting the applicant tag hired
    url = context.applicant_tag_url.replace('app_id', str(applicant_id))
    data = json.dumps({'tags': [tag]})
    await session.post(url=url, headers=context.headers, data=data)


