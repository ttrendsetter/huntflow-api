FROM python:3
WORKDIR /test
COPY .. /test
ENV PYTHONPATH "/test"
RUN pip install --upgrade pip && pip install -r requirements.txt