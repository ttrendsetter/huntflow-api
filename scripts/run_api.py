import aiohttp
from fastapi import FastAPI, Body, Request, Response, Depends
from root.main import (
    get_first_available_applicant,
    add_first_applicant,
    set_applicant_tag,
    get_all_statuses,
    get_hired_tag, get_session
)
from root import schemas

app = FastAPI()


@app.middleware('http')
async def ping_checker(
        request: Request,
        call_next
):
    event = request.headers.get('x-huntflow-event')
    if event == 'PING':
        return Response(status_code=200)

    response = await call_next(request)
    return response


@app.post('/vacancies')
async def vacancies(
        event: schemas.VacancyEvent = Body(embed=True),
        session: aiohttp.ClientSession = Depends(get_session)
):
    if event.vacancy_log.state == 'CREATED':
        if event.vacancy.position:
            applicant = await get_first_available_applicant(event.vacancy, session)
            if applicant:
                await add_first_applicant(applicant, event.vacancy, session)


@app.post('/applicants')
async def applicants(
        event: schemas.ApplicantEvent = Body(embed=True),
        session: aiohttp.ClientSession = Depends(get_session)
):
    if event.applicant_log.type == 'STATUS':
        statuses = await get_all_statuses(session)
        curr_status = event.applicant_log.status.id
        curr_status = list(filter(lambda x: x['id'] == curr_status, statuses))[0]
        if curr_status['type'] == 'hired':
            tag = await get_hired_tag(session)
            await set_applicant_tag(event.applicant.id, tag, session)


@app.get('/')
def main():
    return 'Connected successfully'
