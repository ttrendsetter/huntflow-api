import aiohttp
import asyncio
from root.context import context
import json


async def set_tag():
    """
    Getting all applicant tags and creating a hired tag if it doesn't exist
    """
    async with aiohttp.ClientSession() as session:
        # Get all exists tags
        async with session.get(url=context.get_all_tags, headers=context.headers) as response:
            tags = await response.json()
        tag_names = {tag['name'] for tag in tags['items']}
        # Create the Hired tag if it doesn't exist
        if 'Hired' not in tag_names:
            new_tag = json.dumps({
                'name': 'Hired',
                'color': '000000'
            })
            await session.post(url=context.get_all_tags, headers=context.headers, data=new_tag)


if __name__ == '__main__':
    loop = asyncio.run(set_tag())
